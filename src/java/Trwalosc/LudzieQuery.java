/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Trwalosc;

import org.hibernate.Session;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Transaction;

/**
 *
 * @author student
 */
public class LudzieQuery {
            private Session session = null;
         private List<Radiators> ludzieList = null;
         private Query q = null;
         String html;
         
public String getLudzieLista(boolean OrderByImie) {
         try {         
         org.hibernate.Transaction tx = session.beginTransaction();
         if (OrderByImie) {
         q = session.createQuery("from Radiators order by name");
         } else {
         q = session.createQuery("from Radiators");
         }
         ludzieList = (List<Radiators>) q.list();
         session.close();
         tx.commit();
         html = getListaHTML(ludzieList);
         } catch (HibernateException e) {
         }
         return html;
 }
private String getListaHTML(List<Radiators> lista) {
 String wiersz;
 wiersz = ("<table><tr>");
 wiersz = wiersz.concat(
 "<td><b>ID</b></td>"
 + "<td><b>NAME</b></td>"
 + "<td><b>TYPE</b></td>"
         + "<td><b>CHARACTERISTIC</b></td>"
         + "<td><b>MANUFACTURER</b></td>"
         + "<td><b>NUMBER OF RIBS</b></td>"
         + "<td><b>IS POLISH MANUFACTURER</b></td>"
 ) ;
 wiersz = wiersz.concat("</tr>");
 for (Radiators ldz : lista) {
 wiersz = wiersz.concat("<tr>");
 wiersz = wiersz.concat("<td>" + ldz.getRadiatorId() + "</td>");
 wiersz = wiersz.concat("<td>" + ldz.getName() + "</td>");
 wiersz = wiersz.concat("<td>" + ldz.getType() + "</td>");
 wiersz = wiersz.concat("<td>" + ldz.getCharacteristic() + "</td>");
 wiersz = wiersz.concat("<td>" + ldz.getManufacturer() + "</td>");
 wiersz = wiersz.concat("<td>" + ldz.getNumberOfRibs() + "</td>");
 wiersz = wiersz.concat("<td>" + ldz.getIsPolishManufacturer() + "</td>");
 wiersz = wiersz.concat("</tr>");
 }
 wiersz = wiersz.concat("</table>");
 return wiersz;
 }
 
public void saveRadiator(Radiators radiator) {
        Transaction transaction = null;
        try  {         
            transaction = session.beginTransaction();
            session.save(radiator);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }
public void updateRadiator(Radiators radiator) {
        Transaction transaction = null;
        try {            
            transaction = session.beginTransaction();            
            session.update(radiator);            
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }
  public void deleteRadiator(int id) {
 
        Transaction transaction = null;
        try  {
            transaction = session.beginTransaction();
            Radiators radiator = (Radiators) session.get(Radiators.class, id);
            if (radiator != null) {
                session.delete(radiator);
                System.out.println("user is deleted");
            }
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }
    public LudzieQuery(){
        this.session = HibernateUtil.getSessionFactory().getCurrentSession();
    }
}
